
Wall Theme for Drupal 5.x-1.0
designed by Ana Maria Paz Franchini (Ampafra) for the stage "Development and Management of Internet Sites" carried out in Kinetikon S.r.l. (Turin - Italy) on April-June 2007.

This theme validates as XHTML 1.0 Strict and valid CSS 2.1 (tableless).
Supports two and three columns.
Fixed width.
The Wall template is based on the Barlow template.
Godd for blogs.
Suport all theme options (slogan, logo, primary/secondary links, mission, etc).
Work fine in IE/Firefox-Mozilla/Opera/Konqueror/Safari

Live demo: http://www.kinetikon.com/wall

